$(document).ready(function(){

    var words = $('#poem span');
    words.hide();
    words.eq(0).show().addClass('visible');
    $('#poem').show();


    function nextWord(){
      $('.visible').last().next().show().addClass('visible');
      console.log('next');
    }

    function init() {
      var myFunction = function() {
        nextWord();
        var rand = 100 + Math.round(Math.random() * 800);
        setTimeout(myFunction, rand);
      }
      myFunction();
    }

    $(function() {
        init();
    });

})
