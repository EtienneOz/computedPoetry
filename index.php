<?php include('include/head.inc'); ?>

<!-- 17 x 11 -->

<div id="wrap">

  <?php

    $fileNoms       = file_get_contents('words/noms.txt');
    $fileLiaisons   = file_get_contents('words/connecteurs.txt');
    $fileVerbes     = file_get_contents('words/verbes.txt');
    $fileadjectifs  = file_get_contents('words/adjectifs.txt');
    $fileadverbes   = file_get_contents('words/adverbes.txt');

    $noms       = preg_split('/\R/', $fileNoms);
    $liaisons   = preg_split('/\R/', $fileLiaisons);
    $verbes     = preg_split('/\R/', $fileVerbes);
    $adverbes   = preg_split('/\R/', $fileadverbes);
    $adjectifs  = preg_split('/\R/', $fileadjectifs);

    echo '<div id="poem">';

      for($i = 0; $i <= count($noms)*6; $i++){

        $rand1 = rand(0, count($noms)-2);
        $rand2 = rand(0, count($liaisons)-2);
        $rand3 = rand(0, count($verbes)-2);
        $rand4 = rand(0, count($noms)-2);
        if($i % 8 == 0){
          $rand5 = rand(0, count($adverbes)-2);
        }
        if($i % 16 == 0){
          $rand6 = rand(0, count($adjectifs)-2);
        }
        if($i % 7 == 0){
          $rand7 = rand(0, count($adjectifs)-2);
        }
        if($i % 17 == 0){
          $rand8 = rand(0, count($adverbes)-2);
        }
        if($i % 29 == 0){
          $rand9 = rand(0, count($adverbes)-2);
        }

        $nom          = $noms[$rand1];
        $liaison      = $liaisons[$rand2];
        $verbe        = $verbes[$rand3];
        $nom2         = $noms[$rand4];
        if($i % 8 == 0){
          $adverbe    = $adverbes[$rand5];
        } else {
          $adverbe    = '';
        }
        if($i % 17 == 0){
          $adverbe2   = $adverbes[$rand8];
        } else{
          $adverbe2   = '';
        }
        if($i % 21 == 0){
          $adverbe3     = $adverbes[$rand9];
        } else {
            $adverbe3 = '';
        }
        if($i % 4 == 0){
          $adjectif   = $adjectifs[$rand6];
        } else {
          $adjectif   = '';
        }
        if($i % 7 == 0){
          $adjectif2  = $adjectifs[$rand7];
        } else {
          $adjectif2  = '';
        }

        # remplace "f" par "ve" pour les adjectifs
        # suivant des noms féminims
        if(iconv_strlen($adjectif2) != 0 && mb_substr($adjectif2, -1) == 'f' && strstr($nom2, 'la ')){
          $adjectif2 = str_replace('f', 've', $adjectif2);
        }
        if(iconv_strlen($adjectif) != 0 && mb_substr($adjectif, -1) == 'f' && strstr($nom, 'la ')){
          $adjectif = str_replace('f', 've', $adjectif);
        }

        # rajoute "e" pour les adjectifs
        # suivant des noms féminims
        if(iconv_strlen($adjectif2) != 0 && strstr($nom2, 'la ') && mb_substr($adjectif2, -1) != 'e'){
          $adjectif2 = $adjectif2.'e';
        }
        if(iconv_strlen($adjectif) != 0 && strstr($nom, 'la ') && mb_substr($adjectif, -1) != 'e'){
          $adjectif = $adjectif.'e';
        }

        # ajoute "e" pour les adjectifs
        # suivant des noms féminims avec "l'"
        if (iconv_strlen($adjectif2) != 0 && mb_substr($nom2, -1) == ']' && mb_substr($adjectif2, -1) != 'e') {
          $adjectif2 = $adjectif2.'e';
        }
        if (iconv_strlen($adjectif) != 0 && mb_substr($nom, -1) == ']' && mb_substr($adjectif, -1) != 'e') {
          $adjectif = $adjectif.'e';
        }

        $nom = str_replace(' [f]', '', $nom);
        $nom2 = str_replace(' [f]', '', $nom2);

        // Enregistre le texte généré dans un fichier .txt
        $file = 'poems/poem_'.date("Y-m-d_H:i:s").'.txt';
        file_put_contents($file, $nom." ", FILE_APPEND);
        if (iconv_strlen($adjectif) != 0){
          if (iconv_strlen($adverbe) != 0){
            file_put_contents($file, $adverbe." ", FILE_APPEND);
          }
          file_put_contents($file, $adjectif." ", FILE_APPEND);
        }
        file_put_contents($file, $verbe." ", FILE_APPEND);
        if (iconv_strlen($adverbe2) != 0){
          file_put_contents($file, $adverbe2." ", FILE_APPEND);
        }
        file_put_contents($file, $nom2." ", FILE_APPEND);
        if (iconv_strlen($adjectif2) != 0){
          file_put_contents($file, $adverbe3." ", FILE_APPEND);
          file_put_contents($file, $adjectif2." ", FILE_APPEND);
        }
        file_put_contents($file, $liaison." ", FILE_APPEND);




        // if (iconv_strlen($nom) > 11){
        //   $randSpace  = rand(0, 9);
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $nom        = substr_replace($nom, $spaces, 0, 0);
        // }
        //
        // if (iconv_strlen($nom2) > 11){
        //   $randSpace  = rand(0, 9);
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $nom2       = substr_replace($nom2, $spaces, 0, 0);
        // }
        //
        // # rajoute un nombre d'espaces aléatoires si la liaison est plus petite que 11
        // if(iconv_strlen($liaison) != 11){
        //   $randSpace  = rand(0, 11-(strlen($liaison)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $liaison    = substr_replace($liaison, $spaces, 0, 0);
        //   $liaison    = str_replace(' ', '&nbsp;', $liaison);
        // }
        //
        // # rajoute une espace insécable si la liaison fait 11
        // if(iconv_strlen($liaison) == 11){
        //   $randSpace  = rand(0, 11-(strlen($liaison)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $liaison    = substr_replace($liaison, $spaces, 0, 0);
        //   $liaison    = str_replace(' ', '&nbsp;', $liaison);
        // }
        //
        // # rajoute un nombre d'espaces aléatoires si le nom est plus petit que 11
        // if(iconv_strlen($nom) < 11){
        //   $randSpace  = rand(0, 11-(strlen($nom)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $nom        = substr_replace($nom, $spaces, 0, 0);
        //   $nom        = str_replace(' ', '&nbsp;', $nom);
        // }
        //
        // # rajoute un nombre d'espaces aléatoires si le nom est plus petit que 11
        // if(iconv_strlen($nom2) < 11){
        //   $randSpace  = rand(0, 11-(strlen($nom2)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $nom2       = substr_replace($nom2, $spaces, 0, 0);
        //   $nom2       = str_replace(' ', '&nbsp;', $nom2);
        // }
        //
        // # rajoute une espace insécable si le nom fait 11
        // if(iconv_strlen($nom) == 11){
        //   $nom        = str_replace(' ', '&nbsp;', $nom);
        // }
        //
        // # rajoute une espace insécable si le nom fait 11
        // if(iconv_strlen($nom2) == 11){
        //   $nom2       = str_replace(' ', '&nbsp;', $nom2);
        // }
        //
        // # rajoute un nombre d'espaces aléatoires si le verbe est plus petit que 11
        // if(iconv_strlen($verbe) != 11){
        //   $randSpace  = rand(0, 11-(strlen($verbe)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $verbe      = substr_replace($verbe, $spaces, 0, 0);
        // }
        //
        // # rajoute un nombre d'espaces aléatoires si le verbe est plus petit que 11
        // if(iconv_strlen($adverbe) != 0 && iconv_strlen($adverbe) != 11){
        //   $randSpace  = rand(0, 11-(strlen($adverbe)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $adverbe    = substr_replace($adverbe, $spaces, 0, 0);
        // }
        //
        // # rajoute un nombre d'espaces aléatoires si le verbe est plus petit que 11
        // if(iconv_strlen($adverbe2) != 0 && iconv_strlen($adverbe2) != 11){
        //   $randSpace  = rand(0, 11-(strlen($adverbe2)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $adverbe2    = substr_replace($adverbe2, $spaces, 0, 0);
        // }
        //
        // # rajoute un nombre d'espaces aléatoires si le verbe est plus petit que 11
        // if(iconv_strlen($adverbe3) != 0 && iconv_strlen($adverbe3) != 11){
        //   $randSpace  = rand(0, 11-(strlen($adverbe3)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $adverbe3    = substr_replace($adverbe3, $spaces, 0, 0);
        // }
        //
        // # rajoute un nombre d'espaces aléatoires si le verbe est plus petit que 11
        // if(iconv_strlen($adjectif) != 0 && iconv_strlen($adjectif) != 11){
        //   $randSpace  = rand(0, 11-(strlen($adjectif)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $adjectif   = substr_replace($adjectif, $spaces, 0, 0);
        // }
        //
        // # rajoute un nombre d'espaces aléatoires si le verbe est plus petit que 11
        // if(iconv_strlen($adjectif2) != 0 && iconv_strlen($adjectif2) != 11){
        //   $randSpace  = rand(0, 10-(strlen($adjectif2)));
        //   $spaces     = str_repeat('&nbsp;', $randSpace);
        //   $adjectif2  = substr_replace($adjectif2, $spaces, 0, 0);
        // }

        # on fait marcher la moulinette
        echo '<span>'.$nom.'</span> ';
        if(iconv_strlen($adjectif) != 0){
          if(iconv_strlen($adverbe) != 0){
            echo '<span>'.$adverbe.'</span> ';
          }
          echo '<span>'.$adjectif.'</span> ';
        }
        echo '<span>'.$verbe.'</span> ';
        if(iconv_strlen($adverbe2) != 0){
          echo '<span>'.$adverbe2.'</span> ';
        }
        echo '<span>'.$nom2.'</span> ';
        if(iconv_strlen($adjectif2) != 0){
          if(iconv_strlen($adverbe3) != 0){
            echo '<span>'.$adverbe3.'</span> ';
          }
          echo '<span>'.$adjectif2.'</span> ';
        }
        echo '<span>'.$liaison.'</span> ';
      }

    echo '</div>';

  ?>

</div>

<?php include('include/footer.inc'); ?>
