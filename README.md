# Computed poetry
Un générateur de poèmes, en collaboration avec [Lucas Lejeune](http://cargocollective.com/lucaslejeune/).  
Initialement écrit pour l'exposition **Matrice** à l'[atelier Bek](http://atelier-bek.be).

## usage
Remplacer les listes dans le dossier **words** par les mots de votre choix en respectant leur type.

## exemples
![image1](examples/install.JPG)  
![image1](examples/everyPrints.JPG)  
![image1](examples/print.jpeg)  
![image1](examples/Capture du 2017-03-13 23-04-56.png)  

## live version
[ici](http://etienneozeray.fr/computedPoetry)
